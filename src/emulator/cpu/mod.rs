use std::io::prelude::*;
extern crate rand;
use rand::Rng;
const CHIP8_FONTSET: [u8; 80] = [
  0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
  0x20, 0x60, 0x20, 0x20, 0x70, // 1
  0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
  0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
  0x90, 0x90, 0xF0, 0x10, 0x10, // 4
  0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
  0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
  0xF0, 0x10, 0x20, 0x40, 0x40, // 7
  0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
  0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
  0xF0, 0x90, 0xF0, 0x90, 0x90, // A
  0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
  0xF0, 0x80, 0x80, 0x80, 0xF0, // C
  0xE0, 0x90, 0x90, 0x90, 0xE0, // D
  0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
  0xF0, 0x80, 0xF0, 0x80, 0x80  // F
];

pub struct CPU {
    opcode: u16,
    /**
     * 0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
     * 0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
     * 0x200-0xFFF - Program ROM and work RAM
     **/
    memory: [u8; 4096],
    V: [u8; 16],
    I: u16,
    pc: u16,
    /**
     * Timers are running on 60MHz.
     * opcodes set the timer, which then gets decremented every cycle.
     * At 0 the timer does it's thing (beep in the case of the sound timer)
     **/
    delay_timer: u8,
    sound_timer: u8,
    stack: [u16; 16],
    sp: usize,
    flag: bool,
    last_key: usize,
    pub key: [u8; 16],
    pub screen: super::screen::Screen
}

impl CPU {
    pub fn new() -> Self {
        CPU {
            opcode: 0,
            memory: [0; 4096],
            V: [0; 16],
            I: 0,
            pc: 0,
            delay_timer: 0,
            sound_timer: 0,
            stack: [0; 16],
            sp: 0,
            key: [0; 16],
            screen: super::screen::Screen::new(),
            flag: false,
            last_key: 0
        }
    }

    pub fn initialize(&mut self) {
        //TODO
        self.pc = 0x200; // Program starts here
        self.opcode = 0;
        self.I = 0;
        self.sp = 0;

        // Clear display
        self.screen.clear();
        // Clear stack
        // Clear registers V0-VF
        for i in 0..16 {
            self.stack[i] = 0;
            self.V[i] = 0;
        }
        // Clear memory
        for i in 0..4095 {
            self.memory[i] = 0;
        }

        // Load fontset
        for i in 0..80 {
            self.memory[i] = CHIP8_FONTSET[i];
        }

        //Clear timers
        self.delay_timer = 0;
        self.sound_timer = 0;

        //TODO: load game
        let f = std::fs::File::open("game.c8");
        let mut file: std::fs::File;
        match f {
            Ok(v) => {
                file = v;
            },
            Err(e) => {
                println!("ERROR: Could not open game.c8! {}", e.to_string());
                return;
            }
        }
        let mut contents: [u8; 0xFFF-0x200] = [0;0xFFF-0x200];
        
        let success = file.read(&mut contents);
        match success {
            Ok(v) => {
                println!("Read {} bytes from game.c8", v);
                //Bytes read can't fit into memory.
                if v > 0xFFF-0x200 {
                    println!("ERROR: Program is too big for the CHIP-8 memory.");
                    return;
                }
                for i in 0..(v-1) {
                    self.memory[0x200+i] = contents[i];
                }
                println!("Program loaded into memory!");
            },
            Err(e) => {
                println!("Error reading from file: {}", e.to_string());
            }
        }
    }

    pub fn set_flag(&mut self, last_key: usize) {
        self.flag = true;
        self.last_key = last_key;
    }

    pub fn emulate_cycle(&mut self) {
        // Fetch Opcode
        self.opcode = (self.memory[self.pc as usize] as u16) << 8 | (self.memory[(self.pc + 1) as usize] as u16);
        // Decode Opcode
        println!("{:X}", self.opcode);
        let first = self.opcode & 0xF000;
        match first {
            0x0000 => {
                match self.opcode & 0x0FFF {
                    0x0E0 => {
                        //TODO: Clear display
                        self.screen.clear();
                        self.pc += 2;
                    },
                    0x0EE => {
                        //TODO: Return from subroutine
                        self.sp -= 1;
                        self.pc = self.stack[self.sp as usize] + 2;
                    },
                    _ => {
                        println!("Unimplemented opcode: {}, main was 0x0", self.opcode);
                    }
                }
            },
            0x1000 => {
                //0x1NNN: jump to NNN
                self.pc = self.opcode & 0x0FFF;
            },
            0x2000 => {
                self.stack[self.sp] = self.pc;
                self.sp += 1;
                self.pc = self.opcode & 0x0FFF;
            },
            0x3000 => {
                if self.V[((self.opcode & 0x0F00) >> 8) as usize] == ((self.opcode & 0x00FF) as u8) {
                    self.pc += 4;
                }else {
                    self.pc += 2;
                }
            },
            0x4000 => {
                if self.V[((self.opcode & 0x0F00) >> 8) as usize] != ((self.opcode & 0x00FF) as u8) {
                    self.pc += 4;
                }else {
                    self.pc += 2;
                }
            },
            0x5000 => {
                if self.V[((self.opcode & 0x0F00) >> 8) as usize] == self.V[((self.opcode & 0x00F0) >> 4) as usize] {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            0x6000 => {
                self.V[((self.opcode & 0x0F00) >> 8) as usize] = (self.opcode & 0x00FF) as u8;
                self.pc += 2;
            },
            0x7000 => {
                //self.V[((self.opcode & 0x0F00) >> 8) as usize] += (self.opcode & 0x00FF) as u8;
                self.V[((self.opcode & 0x0F00) >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize].wrapping_add((self.opcode & 0x00FF) as u8);
                self.pc += 2;
            },
            0x8000 => {
                match self.opcode & 0x000F {
                    0x0 => {
                        self.V[(self.opcode & 0x0F00 >> 8) as usize] = self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        self.pc += 2;
                    },
                    0x1 => {
                        self.V[(self.opcode & 0x0F00 >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize] | self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        self.pc += 2;
                    },
                    0x2 => {
                        self.V[(self.opcode & 0x0F00 >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize] & self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        self.pc += 2;
                    },
                    0x3 => {
                        self.V[(self.opcode & 0x0F00 >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize] ^ self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        self.pc += 2;
                    },
                    0x4 => {
                        let val1 = self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        let val2 = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                        if val1 > (0xFF - val2) {
                            self.V[0xF] = 1; // Carry bit (V[F])
                        }else {
                            self.V[0xF] = 0;
                        }
                        //self.V[((self.opcode & 0x0F00) >> 8) as usize] += self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        self.V[((self.opcode & 0x0F00) >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize].wrapping_add(self.V[((self.opcode & 0x00F0) >> 4) as usize]);
                        self.pc += 2;
                    },
                    0x5 => {
                        let val1 = self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        let val2 = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                        if val1 > val2 {
                            self.V[0xF] = 0; // Carry bit (V[F])
                        }else {
                            self.V[0xF] = 1;
                        }
                        //self.V[((self.opcode & 0x0F00) >> 8) as usize] -= self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        self.V[((self.opcode & 0x0F00) >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize].wrapping_sub(self.V[((self.opcode & 0x00F0) >> 4) as usize]);
                        self.pc += 2;
                    },
                    0x6 => {
                        self.V[0xF] = (self.V[((self.opcode & 0x0F00) >> 8) as usize] & 0x1) as u8;
                        self.V[((self.opcode & 0x0F00) >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize] >> 1;
                        self.pc += 2;
                    },
                    0x7 => {
                        let val1 = self.V[((self.opcode & 0x00F0) >> 4) as usize];
                        let val2 = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                        if val2 > val1 {
                            self.V[0xF] = 0; // Carry bit (V[F])
                        }else {
                            self.V[0xF] = 1;
                        }
                        self.V[((self.opcode & 0x0F00) >> 8) as usize] = val1.wrapping_sub(val2);
                        self.pc += 2;
                    },
                    0xE => {
                        self.V[0xF] = (self.V[((self.opcode & 0x0F00) >> 8) as usize] >> 7) as u8;
                        self.V[((self.opcode & 0x0F00) >> 8) as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize] << 1;
                        self.pc += 2;
                    },
                    _ => {
                        println!("Unimplemented opcode: {}, main was 0x8", self.opcode);
                    }
                }
            },
            0x9000 => {
                if self.V[((self.opcode & 0x0F00) >> 8) as usize] != self.V[((self.opcode & 0x00F0) >> 4) as usize] {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            0xA000 => { // ANNN: Sets I to the address NNN
                self.I = self.opcode & 0x0FFF;
                self.pc += 2;
            },
            0xB000 => {
                self.pc = (self.opcode & 0x0FFF) + (self.V[0] as u16);
            },
            0xC000 => {
                let mut rng = rand::thread_rng();
                self.V[((self.opcode & 0x0F00) >> 8) as usize] = (rng.gen_range(0, 256) as u8) & ((self.opcode & 0x00FF) as u8);
                self.pc += 2;
            },
            0xD000 => {
                let x: u8 = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                let y: u8 = self.V[((self.opcode & 0x00F0) >> 4) as usize];
                let height: u8 = (self.opcode & 0x000F) as u8;
                let mut pixel: u8;
                
                self.V[0xF] = 0;
                for yline in 0..(height-1) {
                    pixel = self.memory[(self.I + (yline as u16)) as usize];
                    for xline in 0..7 {
                        if (pixel & (0x80 >> xline)) != 0 {
                            //if gfx[(x + xline + ((y + yline) * 64))] == 1 {
                            let p = self.screen.get_pixel((x as usize +xline as usize) as usize, (y as usize +yline as usize) as usize);
                            if p == 1 {
                                self.V[0xF] = 1;                      
                            }           
                            //gfx[x + xline + ((y + yline) * 64)] ^= 1;
                            println!("{} {} {} {}", x, xline, y, yline);
                            self.screen.set_pixel((x as usize +xline as usize) as usize, (y as usize+yline as usize) as usize, p ^ 1);
                        }
                    }
                }
                
                //drawFlag = true;
                self.screen.set_flag();
                self.pc += 2;
            },
            0xE000 => {
                match self.opcode & 0x00FF {
                    0x9E => {
                        if self.key[self.V[((self.opcode & 0x0F00) >> 8) as usize] as usize] != 0 {
                            self.pc += 4;
                        }else {
                            self.pc += 2;
                        }
                    },
                    0xA1 => {
                        if self.key[self.V[((self.opcode & 0x0F00) >> 8) as usize] as usize] == 0 {
                            self.pc += 4;
                        }else {
                            self.pc += 2;
                        }
                    },
                    _ => {
                        println!("Unimplemented opcode: {}, main was 0xE", self.opcode);
                    }
                }
            },
            0xF000 => {
                match self.opcode & 0x00FF {
                    0x7 => {
                        self.V[((self.opcode & 0x0F00) >> 8) as usize] = self.delay_timer;
                        self.pc += 2;
                    },
                    0xA => {
                        //TODO: wait for key press
                        let mut keyPress: bool = false;
                        for i in 0..15 {
                            if self.key[i] != 0 {
                                self.V[((self.opcode & 0x0F00) >> 8) as usize] = i as u8;
                                keyPress = true;
                            }
                        }
                        if !keyPress {
                            return;
                        }

                        self.pc += 2;
                    },
                    0x15 => {
                        self.delay_timer = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                        self.pc += 2;
                    },
                    0x18 => {
                        self.sound_timer = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                        self.pc += 2;
                    },
                    0x1E => {
                        let x = self.V[((self.opcode & 0x0F00) >> 8) as usize];
                        if (x as u16) + self.I > 0xFFF {
                            self.V[0xF] = 1;
                        }else {
                            self.V[0xF] = 0;
                        }
                        self.I += x as u16;
                        self.pc += 2;
                    },
                    0x29 => {
                        //TODO
                        let x: u16 = self.V[((self.opcode & 0x0F00) >> 8) as usize] as u16;
                        self.I = x*5;
                        self.pc+=2;
                    },
                    0x33 => {
                        self.memory[self.I as usize] = self.V[((self.opcode & 0x0F00) >> 8) as usize] / 100;
                        self.memory[(self.I + 1) as usize] = (self.V[((self.opcode & 0x0F00) >> 8) as usize] / 10) % 10;
                        self.memory[(self.I + 2) as usize] = (self.V[((self.opcode & 0x0F00) >> 8) as usize] % 100) % 10;
                        self.pc += 2;
                    },
                    0x55 => {
                        for i in 0x0..((self.opcode & 0x0F00) >> 8) {
                            self.memory[(self.I + i) as usize] = self.V[i as usize];
                        }

                        self.I += ((self.opcode & 0x0F00) >> 8) + 1;
                        self.pc += 2;
                    },
                    0x65 => {
                        for i in 0x0..((self.opcode & 0x0F00) >> 8) {
                            self.V[i as usize] = self.memory[(self.I + i) as usize];
                        }

                        self.I += ((self.opcode & 0x0F00) >> 8) + 1;
                        self.pc += 2;
                    },
                    _ => {
                        println!("Unimplemented opcode: {}, main was 0xF", self.opcode);
                    }
                }
            },
            _ => {
                println!("Unimplemented opcode: {}", self.opcode);
            }
        }
        // Update timers
        if self.delay_timer > 0 {
            self.delay_timer -= 1;
        }

        if self.sound_timer > 0 {
            if self.sound_timer == 1 {
                println!("BEEP!");
            }
            self.sound_timer -= 1;
        } 
        self.flag = false;
    }
}