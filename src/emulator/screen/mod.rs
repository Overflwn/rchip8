pub const WIDTH: u32 = 800;
pub const HEIGHT: u32 = 600;

// Pixels on the CHIP-8 are either black(0) or white(1)
pub struct Screen {
    pixels: [u8; 64 * 32],
    draw_flag: bool
}

impl Screen {
    pub fn new() -> Self {
        Screen {
            pixels: [0; 64*32],
            draw_flag: false
        }
    }

    pub fn set_flag(&mut self) {
        self.draw_flag = true;
    }

    pub fn set_pixel(&mut self, x: usize, y: usize, val: u8) {
        println!("Setting pixel at {} {}", x, y);
        match x {
            0..=63 => {
                match y {
                    0..=31 => {
                        println!("Inside");
                        self.pixels[64*y + x] = val;
                    },
                    _ => {
                        println!("Y outside.");
                    }
                }
            },
            _ => {
                println!("X outside.");
            }
        }
    }

    pub fn get_pixel(&self, x: usize, y: usize) -> u8 {
        //println!("Getting pixel at {} {}", x, y);
        match x {
            0..=63 => {
                match y {
                    0..=31 => {
                        //println!("Inside");
                        self.pixels[64*y + x]
                    },
                    _ => {
                        //println!("Y outside.");
                        0
                    }
                }
            },
            _ => {
                //println!("X outside.");
                0
            }
        }
    }

    pub fn clear(&mut self) {
        for y in 0..31 {
            for x in 0..63 {
                self.pixels[y*64+x] = 0;
            }
        }
    }
}