#![forbid(unsafe_code)]

use log::error;
use pixels::{wgpu::Surface, Error, Pixels, SurfaceTexture};
use winit::dpi::LogicalSize;
use winit::event::{Event, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;
use winit_input_helper::WinitInputHelper;
pub mod emulator;

const WIDTH: u32 = 320;
const HEIGHT: u32 = 240;
const EMU_WIDTH: u32 = 64;
const EMU_HEIGHT: u32 = 32;
const BOX_SIZE: i16 = 64;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let mut screen = super::emulator::screen::Screen::new();
        screen.set_pixel(1, 1, 1);
        assert_eq!(screen.get_pixel(1, 1), 1);
        screen.set_pixel(64, 1, 1);
        assert_ne!(screen.get_pixel(64, 1), 1);
        screen.set_pixel(1, 32, 1);
        assert_ne!(screen.get_pixel(1, 32), 1);
    }
}

/// Representation of the application state. In this example, a box will bounce around the screen.
struct Chip8 {
    cpu: emulator::cpu::CPU
}

fn main() -> Result<(), Error> {
    env_logger::init();
    let event_loop = EventLoop::new();
    let mut input = WinitInputHelper::new();
    let window = {
        let size = LogicalSize::new(WIDTH as f64, HEIGHT as f64);
        WindowBuilder::new()
            .with_title("Hello Pixels")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop)
            .unwrap()
    };
    let mut hidpi_factor = window.scale_factor();

    let mut pixels = {
        let surface = Surface::create(&window);
        let surface_texture = SurfaceTexture::new(EMU_WIDTH, EMU_HEIGHT, surface);
        Pixels::new(EMU_WIDTH, EMU_HEIGHT, surface_texture)?
    };
    let mut chip8 = Chip8::new();
    chip8.cpu.initialize();

    let keys: [VirtualKeyCode; 4*4] = [
        VirtualKeyCode::Key1, VirtualKeyCode::Key2, VirtualKeyCode::Key3, VirtualKeyCode::Key4,
        VirtualKeyCode::Q, VirtualKeyCode::W, VirtualKeyCode::E, VirtualKeyCode::R,
        VirtualKeyCode::A, VirtualKeyCode::S, VirtualKeyCode::D, VirtualKeyCode::F,
        VirtualKeyCode::Y, VirtualKeyCode::X, VirtualKeyCode::C, VirtualKeyCode::V
    ];

    event_loop.run(move |event, _, control_flow| {

        // Draw the current frame
        if let Event::RedrawRequested(_) = event {
            chip8.draw(pixels.get_frame());
            if pixels
                .render()
                .map_err(|e| error!("pixels.render() failed: {}", e))
                .is_err()
            {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }

        // Handle input events
        if input.update(event) {
            // Close events
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }

            for i in 0..(4*4-1) {
                if input.key_pressed(keys[i]) {
                    chip8.cpu.key[i] = 1;
                }else if input.key_released(keys[i]) {
                    chip8.cpu.key[i] = 0;
                }
            }

            // Adjust high DPI factor
            if let Some(factor) = input.scale_factor_changed() {
                hidpi_factor = factor;
            }

            // Resize the window
            if let Some(size) = input.window_resized() {
                pixels.resize(size.width, size.height);
            }

            // Update internal state and request a redraw
            chip8.update();
            window.request_redraw();
        }
    });
}

impl Chip8 {
    /// Create a new `World` instance that can draw a moving box.
    fn new() -> Self {
        Self {
            cpu: emulator::cpu::CPU::new()
        }
    }

    /// Update the `World` internal state; bounce the box around the screen.
    fn update(&mut self) {
        self.cpu.emulate_cycle();
    }

    /// Draw the `World` state to the frame buffer.
    ///
    /// Assumes the default texture format: [`wgpu::TextureFormat::Rgba8UnormSrgb`]
    fn draw(&self, frame: &mut [u8]) {
        for (i, pixel) in frame.chunks_exact_mut(4).enumerate() {
            let x = i % WIDTH as usize;
            let y = i / WIDTH as usize;
            let pix_on = self.cpu.screen.get_pixel(x, y);
            let rgba = if pix_on == 1 {
                [0xFF,0xFF,0xFF,0xFF]
            } else {
                [0x0,0x0,0x0,0xFF]
            };

            pixel.copy_from_slice(&rgba);
        }
    }
}